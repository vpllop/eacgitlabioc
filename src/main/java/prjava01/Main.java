/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package prjava01;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;

/**
 *
 * @author vpllo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        /**
         * @param args the command line arguments
         */
    
        File f = new File("fitxer.html");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(f))) {
            bw.write(" <h1>DAW2 m08uf4eac4</h1>");bw.newLine(); 
            bw.write("<html>");
            bw.newLine();
            bw.write("  <head>");
            bw.newLine();
            bw.write("    <title>");
            bw.newLine();
            bw.write("      Nova p&agrave;gina  web");
            bw.newLine();
            bw.write("    </title>");
            bw.newLine();
            bw.write("  </head>");
            bw.newLine();
            bw.write("  <body>");
            bw.newLine();
            bw.write("    Nova p&agrave;gina web");
            bw.newLine();
            bw.write("  </body>");
            bw.newLine();
            bw.write("</html>");
            bw.newLine();
            bw.close();
            System.out.println("versi� 0.1 del projecte prjava02");
            try {
                InetAddress adre�a = InetAddress.getLocalHost();
                String hostname = adre�a.getHostName();
                System.out.println("hostname=" + hostname);
                System.out.println("Nom de l'usuari: " + System.getProperty("user.name"));
                System.out.println("Carpeta Personal: " + System.getProperty("user.home"));
                System.out.println("Sistema operatiu: " + System.getProperty("os.name"));
                System.out.println("Versi� OS: " + System.getProperty("os.version"));
            } catch (IOException e) {
                System.out.println("Exception occurred");
            }
        }
    }
}
